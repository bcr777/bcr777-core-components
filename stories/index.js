import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { withKnobs, object, text, number, color } from '@storybook/addon-knobs'

import { IntlProvider } from 'react-intl'
import ThemeProvider from '../src/components/ThemeProvider'
import Navbar from '../src/components/Navbar'
import Topbar from '../src/components/Topbar'
import Appbar from '../src/components/Appbar'
import Footer from '../src/components/Footer'

const NavBarStories = storiesOf('Navbar Components', module)

NavBarStories.addDecorator(withKnobs)

NavBarStories
  .add('without props user', () => {
    return (
      <ThemeProvider>
        <IntlProvider locale="en" messages={{}}>
          <Navbar />
        </IntlProvider>
      </ThemeProvider>
    )
  })
  .add('without custom menu', () => {
    return (
      <ThemeProvider>
        <IntlProvider locale="en" messages={{}}>
          <Navbar bgColor="#ffee00"
                  menus={(
                    <ul>
                      <li><a href="#">sdsa</a></li>
                    </ul>
                  )} />
        </IntlProvider>
      </ThemeProvider>
    )
  })
  .add('without show bg when scroll pos change', () => {
    return (
      <ThemeProvider>
        <IntlProvider locale="en" messages={{}}>
          <div>
            <Navbar bgColor="#ffffff"
                    linkColor="red"
                    menus={(
                      <ul>
                        <li><a href="#">sdsa</a></li>
                      </ul>
                    )} />
            <div style={{ minHeight: '1000px' }}>
              Your Content Here !!
            </div>
          </div>
        </IntlProvider>
      </ThemeProvider>
    )
  })
  .add('with props user', () => {
    const mockAccount = object('User', {
      id: '2g31Za40gaFKD1njgxZM',
      displayName: 'Tistee Boonsuwan',
      points: 1929,
      bonus: 4500,
      packageDailyLifeTime: '2017-12-20T07:52:32.718Z',
      pictureUrl: 'https://graph.facebook.com/v2.6/967354910028987/picture?width=200&height=200',
    })

    return (
      <ThemeProvider>
        <IntlProvider locale="en" messages={{}}>
          <div>
            <Topbar />
            <Navbar user={mockAccount} onClickSignIn={() => action('click SignIn action')}>
              <ul>
                <li><a href="/home">My profile</a></li>
                <li><a href="/abount">Calculators</a></li>
                <li><a href="/abount">Pricing</a></li>
              </ul>
            </Navbar>
          </div>
        </IntlProvider>
      </ThemeProvider>
    )
  })

const AppBarStories = storiesOf('AppBar', module)
AppBarStories.addDecorator(withKnobs)

AppBarStories
  .add('basic', () => {
    return (
      <ThemeProvider>
        <IntlProvider locale="en" messages={{}}>
          <Appbar
            title={text('title', 'Software title name')}
            bgColor={color('bgColor', '#ff0000')}
            winScore={number('winScore', 0)}
            loseScore={number('loseScore', 0)} />
        </IntlProvider>
      </ThemeProvider>
    )
  })

const FooterStories = storiesOf('Footer', module)
FooterStories.addDecorator(withKnobs)

FooterStories
  .add('basic', () => {
    return (
      <ThemeProvider>
        <IntlProvider locale="en" messages={{}}>
          <Footer />
        </IntlProvider>
      </ThemeProvider>
    )
  })

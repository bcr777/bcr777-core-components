import React from 'react'
import styled from 'styled-components'
import { bgColor, color } from 'styled-system'
//language=SCSS prefix=&{ suffix=}
const Button = styled.button`
    padding: 8px 12px;
    border-radius: 2px;
    color: white;
    border: none;
    box-shadow: none;
    font-size: .9rem;
    cursor: pointer;
    ${bgColor}
    ${color}
    &:active, &:focus {
      box-shadow: none;
      outline: none;
    }
`

Button.propTypes = {}
Button.defaultProps = {}

export default Button

import React from 'react'
import omit from 'lodash/omit'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import getThemeColor from '../../utils/getThemeColor'

// language=SCSS prefix=&{ suffix=}
const IconStyled = styled.i`
    vertical-align: middle;
    font-size: ${props => props.fontSize}rem !important;
    color: ${props => getThemeColor(props, props.color)};
    margin: 0 0 .2em 0;
`

const Icon = props => (
  <IconStyled className="material-icons" {...omit(props, ['children'])}>{props.children}</IconStyled>
)

Icon.propTypes = {
  fontSize: PropTypes.number,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}

Icon.defaultProps = {
  fontSize: 1.8,
  children: null,
}

export default Icon

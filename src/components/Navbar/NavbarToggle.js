import React from 'react'
import styled from 'styled-components'

// language=SCSS prefix=dummy{ suffix=}
const NavbarToggle = styled.div`
  cursor: pointer;
  display: none;
  position: relative;
  left: 20px;
  margin-right: 20px;
  //border: 1px solid white;
  color: white;
  //padding: 2px 8px;
  i { 
    color: ${props => props.theme.colors.white};
    font-size: 1.8rem;
    justify-content: center;
    margin: 0;
    //margin-right: 4px;
    vertical-align: middle;
  }
  span {
    font-size: 1rem;
    text-transform: uppercase;
  }
  @media screen and (max-width: 768px){
  display: block; 
  }
`
export default ({ onClick, children }) => (
  <NavbarToggle onClick={onClick}>{children}</NavbarToggle>
)

import styled from 'styled-components'
// language=SCSS prefix=&{ suffix=}
export default styled.div`
  justify-content: flex-end;
  align-items: center;
  flex-basis: 0;
  flex-grow: 1;
  height: 100vh;
  flex-shrink: 0;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1001;
  background-color: ${props => props.bgColor ? props.bgColor : '#250e30'};
  display: ${props => props.active ? 'block' : 'none'};
  
  box-shadow: 0px -3px 36px -12px rgba(0,0,0,0.25);
  > .section-user {
    margin-top: 5em;
    position: relative;
  }
  ul {
    margin-top: 1em;
    padding: 0;
    >li { 
      list-style: none;
      >a {
        padding: 14px 14px;
        display: block;
        font-weight: bold;
        text-align: center;
        text-decoration: none;
        color: #ffffff;
        .active,&:hover,&:focus {
          color: rgba(255,255,255,0.7);
        }
      }
    }
  }
`

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

// language=SCSS prefix=&{ suffix=}
const Text = styled.div`

`

class PackageDailyTimer extends Component {
  constructor() {
    super()
    this.state = {
      intervalRunnerId: 0,
      days: 0,
      seconds: 0,
      minutes: 0,
      hours: 0,
    }
    this.timer = this.timer.bind(this)
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalRunnerId)
  }

  componentDidMount() {

    const intervalRunnerId = setInterval(this.timer, 1000)

    this.setState({
      intervalRunnerId,
    })
  }

  timer() {
    // Get countdown date
    const countDownDate = new Date(this.props.date).getTime()

    // Get todays date and time
    const now = new Date().getTime()

    // Find the distance between now an the count down date
    let distance = countDownDate - now

    // Time calculations for days, hours, minutes and seconds
    const days = Math.floor(distance / (1000 * 60 * 60 * 24))
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    const seconds = Math.floor((distance % (1000 * 60)) / 1000)

    // update state
    this.setState({
      days,
      hours,
      minutes,
      seconds,
    })

    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(this.state.intervalRunnerId)
    }
  }

  render() {
    return [
      <Text key='days'>
        <p>{this.state.days}</p>
        <span>Days</span>
      </Text>,
      <Text key='hours'>
        <p>{this.state.hours}</p>
        <span>Hours</span>
      </Text>,
      <Text key='minutes'>
        <p>{this.state.minutes}</p>
        <span>Minutes</span>
      </Text>,
      <Text key='seconds'>
        <p>{this.state.seconds}</p>
        <span>Seconds</span>
      </Text>,

    ]
  }
}

PackageDailyTimer.propTypes = {
  date: PropTypes.string,
}
PackageDailyTimer.defaultProps = {
  date: '',
}

export default PackageDailyTimer

import React from 'react'
import styled from 'styled-components'
import { FormattedMessage } from 'react-intl'
import PropTypes from 'prop-types'
import truncate from 'lodash/truncate'
import numeral from 'numeral'
import isAfter from 'date-fns/is_after'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import PackageDailyTimer from './PackageDailyTimer'
import messages from './messages'
// language=SCSS prefix=&{ suffix=}
const Credits = styled.section`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(51, 53, 68, 0.7);
    border-radius: 4px;
    margin: 1em 0;
    > div {
      text-align: center;
      display: flex;
      width: 50%;
      flex-direction: column;
      padding: 1em;
      color: white;
      > span {
        font-size: .8rem;
        text-transform: uppercase;
        color: #9696a1;
      }
      > p {
        margin: 3px 0;
        font-size: 1.6rem;
        font-weight: bold;
      }
    }
`

// language=SCSS prefix=&{ suffix=}
const CopyIdButton = styled.button`
    border: 1px solid #462bd9;
    background-color: #462bd9;
    color: white;
    padding: 4px 9px;
    margin-top: 11px;
    margin-left: 3px;
    font-weight: normal;
    text-transform: lowercase;
    border-radius: 4px;
    font-size: .8rem;
`

// language=SCSS prefix=&{ suffix=}
const Profile = styled.section`
    display: flex;
    flex-wrap: nowrap;
    justify-content: flex-start;
    align-items: flex-start;
    p {
      margin: 7px 0;
      &.user-fullname {
        font-size: 1.2rem;
        font-weight: bolder;
        letter-spacing: 1px;
        color: white;
      }
      &.user-id {
        font-weight: normal;
        font-size: .92rem;
        color: ${props => props.theme.colors.grayLight};
        &::selection {
          color: ${props => props.theme.colors.primary};
          background-color: ${props => props.theme.colors.grayLight};
        }
      }
    }

    figure {
      padding: 0;
      margin: 0 1em 0 0;
      > img {
        width: 64px;
        border-radius: 50%;
      }
    }
`
// language=SCSS prefix=&{ suffix=}
const LabelPackageExpire = styled.p`
    font-size: .9rem;
    display: block;
    margin: 1em 0;
    text-align: center;
    color: #aa90b9;
`
// language=SCSS prefix=&{ suffix=}
const TextUserId = styled.textarea`
    background: none;
    border: none;
    outline: none;
    height: auto;
    resize: none;
    text-align: left;
    font-size: 12px;
    letter-spacing: 1px;
    display: inline-block;
    vertical-align: text-top;
    font-weight: bold;
    margin-top: 0;
    width: 185px;
    color: #9696a1;
    padding-top: 0;
`
const NavbarProfile = (props) => {
  const { pictureUrl, id, displayName, points, bonus, packageDailyLifeTime } = props.user
  const isPackageActive = isAfter(new Date(packageDailyLifeTime), new Date())
  const onClickCopyButton = (e) => {
    const copyTextarea = document.getElementById('userId')
    copyTextarea.select()
  }
  return (
    <div>
      <Profile>
        <figure>
          <img src={pictureUrl} alt="avatar" />
        </figure>
        <div>
          <p className="user-fullname">{truncate(displayName, { length: 24 })}</p>
          <p className="user-id">
            <TextUserId readOnly={true} id="userId" value={id} />
          </p>
        </div>
        <CopyToClipboard text={id}
                         onCopy={onClickCopyButton}>
          <CopyIdButton type="button"><FormattedMessage {...messages.btnCopyIdTxt} /></CopyIdButton>
        </CopyToClipboard>
      </Profile>
      {isPackageActive &&
      <LabelPackageExpire><FormattedMessage {...messages.labelPackageExpire} /></LabelPackageExpire>}
      <Credits>
        {
          isPackageActive ? (<PackageDailyTimer date={packageDailyLifeTime} />) : [
            <div key="label-point">
              <FormattedMessage {...messages.labelPoints} />
              <p>{numeral(points).format('0,0')}</p>
            </div>
            ,
            <div key="label-bonus">
              <FormattedMessage {...messages.labelBonus} />
              <p>{numeral(bonus).format('0,0')}</p>
            </div>,
          ]
        }
      </Credits>

    </div>
  )
}
NavbarProfile.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string,
    displayName: PropTypes.string,
    bonus: PropTypes.number,
    points: PropTypes.number,
    packageDailyLifeTime: PropTypes.string,
    pictureUrl: PropTypes.string,
  }),
  onClickSignIn: PropTypes.func,
}
NavbarProfile.defaultProps = {
  onClickSignIn: () => {
    window.location.href = '/sign-in'
  },
  user: null,
}
export default NavbarProfile

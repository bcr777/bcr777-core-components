import { defineMessages } from 'react-intl'

export default defineMessages({
  labelMenu: {
    id: 'bcr777-core.components.Navbar.labelMenu',
    defaultMessage: 'Menu',
  },
  btnSignInTxt: {
    id: 'bcr777-core.components.Navbar.btnSignInTxt',
    defaultMessage: 'Sign In',
  },
  btnSignUpTxtShort: {
    id: 'bcr777-core.components.Navbar.btnSignUpTxtShort',
    defaultMessage: 'Sign Up',
  },
  linkSignOutTxt: {
    id: 'bcr777-core.components.Navbar.linkSignOutTxt',
    defaultMessage: 'Logout',
  },
  linkMyProfileTxt: {
    id: 'bcr777-core.components.Navbar.linkMyProfileTxt',
    defaultMessage: 'Profile',
  },
  linkHomeTxt: {
    id: 'bcr777-core.components.Navbar.linkHomeTxt',
    defaultMessage: 'Home',
  },
  linkPricingTxt: {
    id: 'bcr777-core.components.Navbar.linkPricingTxt',
    defaultMessage: 'Pricing',
  },
  linkAgentTxt: {
    id: 'bcr777-core.components.Navbar.linkAgentTxt',
    defaultMessage: 'Agent',
  },
  linkContactTxt: {
    id: 'bcr777-core.components.Navbar.linkContactTxt',
    defaultMessage: 'Contact us',
  },
  linkCalculatorsTxt: {
    id: 'bcr777-core.components.Navbar.linkCalculatorsTxt',
    defaultMessage: 'Calculators',
  },
  linkVipTxt: {
    id: 'bcr777-core.components.Navbar.linkVipTxt',
    defaultMessage: 'VIP Member',
  },
  btnCopyIdTxt: {
    id: 'bcr777-core.components.Navbar.btnCopyIdTxt',
    defaultMessage: 'copy user id',
  },
  labelId: {
    id: 'bcr777-core.components.Navbar.labelId',
    defaultMessage: 'ID',
  },
  labelPackageExpire: {
    id: 'bcr777-core.components.Navbar.labelPackageExpire',
    defaultMessage: 'Your package will expire in'
  },
  labelBonus: {
    id: 'bcr777-core.components.Navbar.labelBonus',
    defaultMessage: 'Bonus',
  },
  labelCredit: {
    id: 'bcr777-core.components.Navbar.labelCredit',
    defaultMessage: 'credit',
  },
  labelPoints: {
    id: 'bcr777-core.components.Navbar.labelPoints',
    defaultMessage: 'Points',
  },
})

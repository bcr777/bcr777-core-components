import styled from 'styled-components'
import media from 'styled-media-query'
// language=SCSS prefix=&{ suffix=}
export default styled.div`
    background: transparent;
    border: none;
    outline: none;
    cursor: pointer;
    display: block;
    position: absolute;
    top: 38px;
    right: 22px;
    i { 
      color: ${props => props.theme.colors.grayLight};
      font-size: 1.8rem;
    }
   ${media.lessThan('medium')`
      display: block;
      order: 1;
      top: 22px;
      right: 18px;
  `}
`

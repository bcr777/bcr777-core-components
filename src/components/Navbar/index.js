import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'
import map from 'lodash/map'
import get from 'lodash/get'
import numeral from 'numeral'
// import anime from 'animejs'
import isBefore from 'date-fns/is_before'
import truncate from 'lodash/truncate'
import NavbarToggle from './NavbarToggle'
import NavProfile from './NavProfile'
import Icon from '../Icon'
import Button from '../Button'
import messages from './messages'
// language=SCSS prefix=dummy{ suffix=}
const Nav = styled.nav`
    min-height: 3.5rem;
    padding: .7em 0;
    display: flex;
    align-items: center;
    z-index: 10;
    justify-content: flex-start;
    background-color: ${props => props.bgColor ? props.bgColor : 'transparent'};
    @media screen and (max-width: 412px){
      justify-content: space-around;
      padding: .5em 0;
      transition: all 0.3s ease;
    }
`
// language=SCSS prefix=dummy{ suffix=}
const Logo = styled.a`
    display: flex;
    align-items: center;
    height: 3.5rem;
    padding: 0 0 0 1.5rem;
    > img {
      width: 96px;
    }
    @media screen and (max-width: 768px) {
      flex-grow: 1;
    }
    @media screen and (max-width: 414px) {
      > img {
        width: 74px;
      }
    }
`

// language=SCSS prefix=&{ suffix=}
const InlineMenus = styled.div.attrs({
  className: 'navbar-menus',
})`
  display: inline-block;
  flex-grow: 1;
  margin-left: 20px;
  ul {
    display: inline-block;
    margin: 0;
    padding: 0;
  > li {
    display: inline-block;
    margin: 0 3px;
    > a {
      padding: 12px 11px;
      text-decoration: none;
      color: white;
      border-radius: 4px;
      &:hover {
        background-color: rgba(0,0,0,.2);
        color: ${props => props.linkColor ? props.linkColor : props.theme.colors.themeColor1};
      }
    }
  }
}
@media screen and (max-width: 768px){
    display: ${props => props.toggle ? 'block' : 'none'};
    width: 100%;
    position: absolute;
    left: 0;
    margin: 0;
    top: 110px;
    z-index: 99;
    background: rgb(20,19,37);
    ul {
      margin-top: 0;
      width: 100%;
      height: 100%;
      padding: 1em 0;
      > li {
        display: block;
      }
      > li > a {
        display: block;
        width: 100%;
        text-align: center;
      }
    }
  } 
`
// language=SCSS prefix=&{ suffix=}
const NavbarRight = styled.div`
    margin: 0 12px;
`
// language=SCSS prefix=&{ suffix=}
const NavbarAvatar = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    .content {
      padding: 0 6px;
      line-height: 24px;
    }

    img {
      border-radius: 50%;
      width: 40px;
    }

    h3 {
      font-size: 1rem;
      text-transform: uppercase;
    }

    p {
      font-size: .88rem;
    }

    h3, p {
      color: rgba(255, 255, 255, .7);
      margin: 0;
    }

    @media screen and (max-width: 414px) {
      h3, p {
        font-size: .8rem;
      }
    }
`
// language=SCSS prefix=&{ suffix=}
const Credits = styled.b`
  color: ${p => p.theme.colors.themeColor1};
`
// language=SCSS prefix=&{ suffix=}
const ButtonSignIn  = styled(Button)`
  padding: 12px 11px;
  background: #735df5;
  min-width: 90px;
  text-transform: uppercase;
`


class Navbar extends Component {
  constructor() {
    super()
    this.state = {
      toggle: false,
      scrollPos: 0,
    }
  }

  toggleNavbar() {
    this.setState({
      toggle: !this.state.toggle,
    })
  }

  renderMenusItems() {
    const links = [
      {
        msgId: 'linkCalculatorsTxt',
        href: '/calculators',
      },
      {
        msgId: 'linkPricingTxt',
        href: '/pricing',
      },
      {
        msgId: 'linkAgentTxt',
        href: '/dealership',
      },
      {
        msgId: 'linkContactTxt',
        href: 'https://portal.bcr777.club/contact',
      },
    ]
    return map(links, (link) => <li key={link.msgId}>
      <a onClick={link.onClick} href={link.href}>
        <FormattedMessage {...messages[link.msgId]} />
      </a>
    </li>)
  }

  getCreditsLabel() {
    const { user } = this.props
    if (user.packageDailyLifeTime !== null && isBefore(new Date(), user.packageDailyLifeTime)) {
      return 'Unlimited'
    }
    return numeral(+user.points + user.bonus).format('0,0')
  }

  render() {
    return (
      <Nav bgColor={this.props.bgColor}>
        <NavbarToggle onClick={() => this.toggleNavbar()}>
          <Icon color="white">
            {this.state.toggle ? 'close' : 'menu'}
          </Icon>
        </NavbarToggle>
        <Logo href={this.props.logoUrl || '/'}>
          <img src="https://bcr777.club/static/images/svg/bcr777-logo-light.svg" alt="logo" />
        </Logo>
        <InlineMenus toggle={this.state.toggle} linkColor={this.props.linkColor}>
          {this.props.menus ? this.props.menus : (
            <ul>
              {this.renderMenusItems()}
            </ul>
          )}
        </InlineMenus>
        <NavbarRight>
          {
            (get(this.props, 'user.id', null)) !== null ?
              (
                <NavbarAvatar onClick={this.props.onClickProfile}>
                  <img src={this.props.user.pictureUrl} alt="avatar" />
                  <div className="content">
                    <h3>{truncate(this.props.user.displayName, { length: 15 })}</h3>
                    <p><FormattedMessage {...messages.labelCredit} /> :<Credits>{this.getCreditsLabel()}</Credits></p>
                  </div>
                </NavbarAvatar>
              ) : (
                <ButtonSignIn onClick={this.props.onClickSignIn}>
                  <FormattedMessage {...messages.btnSignInTxt}/>
                </ButtonSignIn>
              )
          }
        </NavbarRight>
      </Nav>
    )
  }
}

Navbar.propTypes = {
  logoUrl: PropTypes.string,
  user: NavProfile.propTypes.user,
  bgColor: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  onClickProfile: PropTypes.func,
  onClickSignIn: PropTypes.func,
}
Navbar.defaultProps = {
  logoUrl: '/',
  user: null,
  bgColor: 'rgb(24, 24, 37)',
  menus: false,
  linkColor: null,
  onClickProfile: () => window.location = '/settings',
  onClickSignIn: () => window.location = '/login',
}

export default Navbar

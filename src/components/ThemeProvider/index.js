import React from 'react'
import PropTypes from 'prop-types'
import merge from 'lodash/merge'
import { Provider as ThemeProvider } from 'rebass'

const defaultTheme = {
  colors: {
    primary: '#735df5',
    success: '#00FF6D',
    warning: '#FECE00',
    error: '#ff5f7c',
    info: '#549fe2',
    white: '#f0f0f0',
    darken: '#4b4b4b',
    grayLight: '#d2d2d2',
    themeColor1: '#735df5',
    themeColor2: '#1D2441',
    themeColor3: '#60BAC3',
    themeColor4: '#bf58be',
    themeColor5: '#09132C',
    themeColor6: '#3B4E6C',
  },
  space: [0, 6, 12, 18, 24],
  breakpoints: ['32em', '48em', '64em'],
  font: 'Roboto,"Helvetica Neue",Helvetica,Arial,sans-serif',
  fontHeading: 'Prompt,"Helvetica Neue",Helvetica,Arial,sans-serif;',
  fontSizes: [
    12, 16, 18, 24, 36, 48, 72,
  ],
  weights: [
    400, 600,
  ],
}

// Create a GreenSection component that renders its children wrapped in
// a ThemeProvider with a green theme
export const DefaultTheme = ({ children, themeOptions }) => (
  <ThemeProvider theme={merge(defaultTheme, themeOptions)}>
    {children}
  </ThemeProvider>
)

DefaultTheme.propTypes = {
  themeOptions: PropTypes.object,
}
DefaultTheme.defaultProps = {
  themeOptions: {},
}

export default DefaultTheme

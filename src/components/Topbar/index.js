import React, { Component } from 'react'
import { findDOMNode } from 'react-dom'
import styled from 'styled-components'
import get from 'lodash/get'
import { intlShape, injectIntl } from 'react-intl'
import Icon from '../Icon'
import LanguageDropdown, { languages } from '../LanguageDropdown'
// language=SCSS prefix=&{ suffix=}
const Wrapper = styled.div.attrs({
  className: 'topbar',
})`
    border: 0;
    width: 100%;
    background: ${props => props.theme.colors.themeColor1};
    height: 40px;
    display: flex;
    line-height: 40px;
    justify-content: space-between;
    padding: 0 1em;
    box-sizing: border-box;
    color: white;
    > .language {
      padding: 0;
      background: none;
      border: 0;
      color: white;
      cursor: pointer;
      outline: none;
      &:hover {
        outline: none;
      }
      > i {
        margin-left: 4px;
        font-size: 1rem !important;
      }
    }
    > .mail {
      > i {
        margin-right: 4px;
        font-size: 1rem !important;
      }
      a {
        color: white;
        text-decoration: none;
        &:hover {
          color: #dae1ff;
        }
      }
    }
`

class Topbar extends Component {
  constructor() {
    super()
    this.state = {
      dropdownVisible: false,
    }
    this.handleClickOutside = this.handleClickOutside.bind(this)
  }

  toggleDropDown() {
    if (!this.state.dropdownVisible) {
      document.addEventListener('click', this.handleClickOutside, false)
    } else {
      document.removeEventListener('click', this.handleClickOutside, false)
    }
    this.setState(prevState => ({
      dropdownVisible: !prevState.dropdownVisible,
    }))
  }

  /**
   * Alert if clicked on outside of element
   */
  handleClickOutside(e) {
    if (this.state.dropdownVisible && findDOMNode(this.languageDropdown).contains(e.target)) {
      return false
    }
    this.toggleDropDown()
  }

  render() {
    const { intl } = this.props
    return (
      <Wrapper>
        <p className="mail">
          <Icon>mail</Icon>
          <a href="mailto:support@bcr777.club">support@bcr777.club</a>
        </p>
        <button type="button" onClick={() => this.toggleDropDown()} className="language">
          {get(languages, intl.locale, 'en')}
          <Icon>keyboard_arrow_down</Icon>
        </button>
        <LanguageDropdown
          activeLang={intl.locale}
          show={this.state.dropdownVisible}
          ref={node => {
            this.languageDropdown = node
          }}
        />
      </Wrapper>
    )
  }
}

Topbar.propTypes = {
  intl: intlShape,
}
export default injectIntl(Topbar)

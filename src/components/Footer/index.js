import React from 'react'
import styled from 'styled-components'
import { FormattedMessage } from 'react-intl'
import messages from './messages'
// language=SCSS prefix=&{ suffix=}
const Wrapper = styled.footer`
    border: 0;
    margin: 0;
    width: 100%;
    height: 60px;
    line-height: 60px;
    background: transparent;
    display: flex;
    justify-content: space-between;
    padding: 0 1em;
    box-sizing: border-box;
    color: white;
    > p {
      margin: 0;
      width: 100%;
      text-align: center;
    }
    .copyright {
      font-size: 1rem;
      @media screen and (max-width: 414px) {
        font-size: 0.85rem;
      }
    }
`
export default () => {

  return (
    <Wrapper>
      <p className="copyright">
        <FormattedMessage {...messages.labelCopyright} values={{ start: '2016', end: (new Date()).getFullYear() }} />
      </p>
    </Wrapper>
  )
}

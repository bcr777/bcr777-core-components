import { defineMessages } from 'react-intl'

export default defineMessages({
  labelCopyright: {
    id: 'bcr777-core.components.Footer.labelCopyright',
    defaultMessage: '© Copyright BCR777 {start} - {end}. All Rights Reserved.',
  },
})

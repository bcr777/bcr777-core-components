import styled from 'styled-components'

// language=SCSS prefix=&{ suffix=}
const Divider = styled.hr`
    border: 0;
    height: 1px;
    border-top: 1px solid rgba(27, 13, 39, 0.8);
    margin: 10px 0
`

export default Divider

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import shortid from 'shortid'
import styled from 'styled-components'
import map from 'lodash/map'
// language=SCSS prefix=&{ suffix=}
const Wrapper = styled.ul.attrs({
  className: 'dropdown-language',
})`
    display: ${props => props.show ? 'block' : 'none'};
    position: absolute;
    z-index: 100;
    width: 160px;
    height: auto;
    padding: 4px 0;
    margin: 0;
    background: rgba(255, 255, 255, 1);
    border-radius: 4px;
    right: 10px;
    top: 55px;
    box-shadow: 0px 0px 14px 2px rgba(100, 100, 100, 0.2);
    
    &:before {
      content: "";
      width: 0;
      height: 0;
      position: absolute;
      border-left: 12px solid transparent;
      border-right: 12px solid transparent;
      border-bottom: 10px solid #FFFFFF;
      top: -10px;
      right: 24px;
    }
`
// language=SCSS prefix=&{ suffix=}
const DropdownItem = styled.li`
    width: 100%;
    border-bottom: 1px solid #e0e0e0;
    list-style: none;
    &:last-child {
      border-bottom: 0;
    }
    > a {
      text-align: center;
      width: 100%;
      display: block;
      text-decoration: none;
      color: ${props => props.active ? 'white' : props.theme.colors.themeColor6};
      background: ${props => props.active ? props.theme.colors.themeColor1 : 'white'};
      &:hover {
        text-decoration: none;
        color:  ${props => props.active ? 'white' : props.theme.colors.themeColor1}
      }
    }
`

export const languages = {
  th: 'ภาษาไทย',
  en: 'English',
  // ko: '한국어',
  // lo: 'ພາສາລາວ',
}

class LanguageDropdown extends Component {
  render() {
    const url = typeof window === 'undefined' ? '/' : window.location.href.split('?')[0]
    const { activeLang, show } = this.props
    return (
      <Wrapper show={show}>
        {map(languages, (label, lang) => (
          <DropdownItem
            active={activeLang === lang}
            key={shortid.generate()}
          >
            <a href={activeLang === lang ? '#' : `/locale/switch?lang=${lang}&redirectTo=${url}`}>{label}</a>
          </DropdownItem>))}
      </Wrapper>
    )
  }
}

LanguageDropdown.propTypes = {
  activeLang: PropTypes.string.isRequired,
  show: PropTypes.bool.isRequired,
}
export default LanguageDropdown

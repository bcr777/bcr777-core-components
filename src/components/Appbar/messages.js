import { defineMessages } from 'react-intl'

export default defineMessages({
  labelWin: {
    id: 'bcr777-core.components.Appbar.labelWin',
    defaultMessage: 'Wins',
  },
  labelLose: {
    id: 'bcr777-core.components.Appbar.labelLose',
    defaultMessage: 'Lose',
  },
})

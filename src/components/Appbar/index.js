import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'
import truncate from 'lodash/truncate'
import numeral from 'numeral'
import messages from './messages'
import Icon from '../Icon'

// language=SCSS prefix=&{ suffix=}
const Wrapper = styled.header`
    width: 100%;
    height: auto;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
    align-items: center;
    line-height: 0;
    background-color: ${props => props.bgColor};
    color: ${props => getContrastYIQ(props.bgColor.replace('#', ''),'rgba')};
`
// language=SCSS prefix=&{ suffix=}
const ButtonNavigateBack = styled.button`
    background-color: transparent;
    border: none;
    outline: none;
    box-shadow: none;
    cursor: pointer;
    > i {
      margin: 0;
    }
`

// language=SCSS prefix=&{ suffix=}
const Title = styled.h4`
    font-size: 1.2rem;
    margin: 0;
    padding: 1em 0;
    flex-grow: 2;
    font-weight: 400;
    letter-spacing: 1px;
`

// language=SCSS prefix=&{ suffix=}
const BoxWinLose = styled.ul`
    margin: 0;
    min-width: 160px;
    height: 100%;
    text-align: right;
    display: flex;
    justify-content: center;
    padding: 15px 0 0;
    background: rgba(0,0,0,.14);
    > li {
      list-style: none;
      display: inline-block;
      text-align: center;
      vertical-align: middle;
      margin: 0 0.5rem;
      line-height: 30px;
      &.separator {
        font-family: sans-serif;
        position: relative;
        font-size: 1.4em;
        margin-top: 5px;
      }
      > p {
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
        line-height: 1rem;
        font-size: 1.5rem;
        font-weight: 400;
        margin: 0;
        border-bottom: 0.3rem !important;
      }
      > span {
        font-size: 0.75rem;
      }
    }
`

function getContrastYIQ(hexcolor, type = 'name') {
  const r = parseInt(hexcolor.substr(0, 2), 16)
  const g = parseInt(hexcolor.substr(2, 2), 16)
  const b = parseInt(hexcolor.substr(4, 2), 16)
  const yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000
  if(type==='rgba') {
    return (yiq >= 128) ? 'rgba(0,0,0,.7)' : 'rgba(255,255,255,.7)'
  }else {
    return (yiq >= 128) ? 'black' : 'white'
  }
}

const Appbar = (props) => {
  const { title, winScore, loseScore, onClickNavigateBack, bgColor } = props
  return (
    <Wrapper bgColor={bgColor}>
      <ButtonNavigateBack onClick={onClickNavigateBack}>
        <Icon color={getContrastYIQ(bgColor.replace('#', ''))} fontSize={2.3}>navigate_before</Icon>
      </ButtonNavigateBack>
      <Title>{truncate(title, { 'length': 24 })}</Title>
      <BoxWinLose>
        <li>
          <p className="win">{numeral(+winScore).format('0,0')}</p>
          <FormattedMessage {...messages.labelWin} />
        </li>
        <li className="separator">/</li>
        <li>
          <p className="lose">{numeral(+loseScore).format('0,0')}</p>
          <FormattedMessage {...messages.labelLose} />
        </li>
      </BoxWinLose>
    </Wrapper>
  )
}

Appbar.propTypes = {
  title: PropTypes.string,
  bgColor: PropTypes.string,
  winScore: PropTypes.number,
  loseScore: PropTypes.number,
  onClickNavigateBack: PropTypes.func,
}
Appbar.defaultProps = {
  title: 'Software title name',
  winScore: 0,
  loseScore: 0,
  bgColor: 'transparent',
  onClickNavigateBack: () => null,
}

export default Appbar
